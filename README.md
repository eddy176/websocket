# Websocket

A web application for tracking cryptocurrencies. It uses vue and node for the front and backend. Websockets for real time updates. It is hosted using Heroku and an API is used to track the cryptos listed on the site. I also use CSS and HTML for designing the whole application.

![Crypto](images/crypto.png)
